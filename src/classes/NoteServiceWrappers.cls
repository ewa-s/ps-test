/**
 * Created by ewasm on 15.09.2018.
 */

global with sharing class NoteServiceWrappers {

    global with sharing class NoteWrapper {
        global String id;
        global String title;
        global String content;
        global Datetime createdDate;
        global Datetime modifiedDate;
        global Integer version;
        global Boolean deleted;


        global NoteWrapper(PS_Note__c note) {
            id = note.Id;
            title = note.Title__c;
            content = note.Content__c;
            createdDate = note.CreatedDate;
            modifiedDate = note.Modified_Date__c;
            version = (Integer)note.Version__c;
            deleted = note.Deleted__c;
        }

        global NoteWrapper(PS_Note_History__c note) {
            id = note.Id;
            title = note.Title__c;
            content = note.Content__c;
            createdDate = note.Version_Created_Date__c;
            version = (Integer)note.Version__c;
        }
    }

    global class NewNote {
        global String title;
        global String content;

        global NewNote(String title, String content) {
            this.title = title;
            this.content = content;
        }
    }

    global class NotesServiceResult {
        global Boolean success;
        global List<String> errors;
        global Object data;

        global NotesServiceResult() {
            errors = new List<String>();
        }
    }
}