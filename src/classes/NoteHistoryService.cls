/**
 * Created by ewasm on 15.09.2018.
 */
@RestResource(urlMapping='/PSNoteHistoryService/*')
global with sharing class NoteHistoryService {
    @HttpGet
    global static void getNoteHistory() {
        NoteServiceWrappers.NotesServiceResult result = new NoteServiceWrappers.NotesServiceResult();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String noteId = req.requestURI.substringAfterLast('PSNoteHistoryService').replace('/','');
        result.success = true;

        if (String.isBlank(noteId)) {
            result.success = false;
            result.errors.add('Id required');
        }
        else {
            try {
                PS_Note__c currentNote = [SELECT Id, Title__c, Content__c, CreatedDate, Modified_Date__c, Version__c, Deleted__c,
                                                (SELECT Title__c, Content__c, Version_Created_Date__c, Version__c
                                                FROM PS_Notes_History__r
                                                ORDER BY Version__c DESC)
                                            FROM PS_Note__c
                                            WHERE Id = :noteId];
                Map<String,Object> noteWithHistory = new Map<String, Object> ();
                noteWithHistory.put('currentVersion', new NoteServiceWrappers.NoteWrapper(currentNote));
                noteWithHistory.put('noteHistory', new List<NoteServiceWrappers.NoteWrapper>());
                for (PS_Note_History__c oldVersion : currentNote.PS_Notes_History__r) {
                    List<NoteServiceWrappers.NoteWrapper> historyList = (List<NoteServiceWrappers.NoteWrapper>)noteWithHistory.get('noteHistory');
                    historyList.add(new NoteServiceWrappers.NoteWrapper(oldVersion));
                    noteWithHistory.put('noteHistory',historyList);
                }
                result.data = noteWithHistory;
            }
            catch (Exception ex) {
                result.success = false;
                result.errors.add(ex.getMessage());
            }
        }

        res.statusCode = result.success ? 200 : 500;
        res.responseBody = Blob.valueOf(JSON.serialize(result));
        res.addHeader('Content-Type', 'application/json');
    }
}