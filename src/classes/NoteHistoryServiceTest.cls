/**
 * Created by ewasm on 16.09.2018.
 */
@IsTest
public with sharing class NoteHistoryServiceTest {

    @IsTest
    private static void shouldGetNoteHistory() {
        PS_Note__c note = NoteTestUtil.insertNote('New Title', 'New Content');
        PS_Note_History__c noteHistory = NoteTestUtil.insertNoteHistory(note.Id, 'Old Title', 'Old Content');

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNoteHistoryService/' + note.Id;
        RestContext.request.httpMethod = 'GET';
        RestContext.response = new RestResponse();

        NoteHistoryService.getNoteHistory();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        Object resultDataObject = (Object)result.get('data');
        Map<String,Object> resultDataMap = (Map<String,Object>) resultDataObject;
        Object currentVersionObject = resultDataMap.get('currentVersion');
        NoteServiceWrappers.NoteWrapper currentNoteVersion = (NoteServiceWrappers.NoteWrapper)(JSON.deserialize(JSON.serialize(currentVersionObject),NoteServiceWrappers.NoteWrapper.class));

        Object oldVersionObject = resultDataMap.get('noteHistory');
        List<NoteServiceWrappers.NoteWrapper> oldNoteVersion = (List<NoteServiceWrappers.NoteWrapper>)(JSON.deserialize(JSON.serialize(oldVersionObject),List<NoteServiceWrappers.NoteWrapper>.class));

        System.assertEquals(note.Title__c, currentNoteVersion.title);
        System.assertEquals(note.Content__c, currentNoteVersion.content);
        System.assertEquals(noteHistory.Title__c, oldNoteVersion[0].title);
        System.assertEquals(noteHistory.Content__c, oldNoteVersion[0].content);
    }
}