/**
 * Created by ewasm on 16.09.2018.
 */
@IsTest
public with sharing class NoteServiceTest {

    @IsTest
    private static void shouldGetAllNotes() {
        PS_Note__c note1 = NoteTestUtil.insertNote('Title note 1', 'Content note 1');
        PS_Note__c note2 = NoteTestUtil.insertNote('Title note 2', 'Content note 2');
        List<NoteServiceWrappers.NoteWrapper> listOfNoteWrappers = new List<NoteServiceWrappers.NoteWrapper>();
        listOfNoteWrappers.add(new NoteServiceWrappers.NoteWrapper(note1));
        listOfNoteWrappers.add(new NoteServiceWrappers.NoteWrapper(note2));
        NoteServiceWrappers.NotesServiceResult expectedResult = new NoteServiceWrappers.NotesServiceResult();
        expectedResult.success = true;
        expectedResult.data = listOfNoteWrappers;

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/';
        RestContext.request.httpMethod = 'GET';
        RestContext.response = new RestResponse();

        NoteService.getNotes();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());

        List<NoteServiceWrappers.NoteWrapper> resultWrapper = new List<NoteServiceWrappers.NoteWrapper>();
        for (Object resultObject : (List<Object>)result.get('data')) {
            NoteServiceWrappers.NoteWrapper noteWrapper = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(JSON.serialize(resultObject), NoteServiceWrappers.NoteWrapper.class);
            resultWrapper.add(noteWrapper);
        }

        System.assert((Boolean)result.get('success'));
        System.assertEquals(0, ((List<Object>)result.get('errors')).size());
        System.assert(listContainsTitle(resultWrapper, 'Title note 1'));
        System.assert(listContainsTitle(resultWrapper, 'Title note 2'));
        System.assert(listContainsContent(resultWrapper, 'Content note 1'));
        System.assert(listContainsContent(resultWrapper, 'Content note 2'));
    }

    private static Boolean listContainsTitle(List<NoteServiceWrappers.NoteWrapper> notesList, String title) {
        for (NoteServiceWrappers.NoteWrapper note : notesList) {
            if (note.title == title) {
                return true;
            }
        }
        return false;
    }

    private static Boolean listContainsContent(List<NoteServiceWrappers.NoteWrapper> notesList, String content) {
        for (NoteServiceWrappers.NoteWrapper note : notesList) {
            if (note.content == content) {
                return true;
            }
        }
        return false;
    }

    @IsTest
    private static void shouldGetNoteById() {
        PS_Note__c note = NoteTestUtil.insertNote('Title note', 'Content note');

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/' + note.Id;
        RestContext.request.httpMethod = 'GET';
        RestContext.response = new RestResponse();

        NoteService.getNotes();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        Object resultDataObject = (Object)result.get('data');
        NoteServiceWrappers.NoteWrapper resultWrapper = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(JSON.serialize(resultDataObject), NoteServiceWrappers.NoteWrapper.class);

        System.assert((Boolean)result.get('success'));
        System.assertEquals(0, ((List<Object>)result.get('errors')).size());
        System.assertEquals(note.Title__c, resultWrapper.title);
        System.assertEquals(note.Content__c, resultWrapper.content);
        System.assertEquals(note.Version__c, resultWrapper.version);
        System.assertEquals(note.Deleted__c, resultWrapper.deleted);
    }

    @IsTest
    private static void shouldInsertNote() {
        String title = 'New Note Title';
        String content = 'New Note Content';
        NoteServiceWrappers.NewNote reqBody = new NoteServiceWrappers.NewNote(title, content);

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/';
        RestContext.request.httpMethod = 'POST';
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(reqBody));
        RestContext.response = new RestResponse();

        NoteService.createNote();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        Object resultDataObject = (Object)result.get('data');
        NoteServiceWrappers.NoteWrapper resultWrapper = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(JSON.serialize(resultDataObject), NoteServiceWrappers.NoteWrapper.class);

        System.assert((Boolean)result.get('success'));
        System.assertEquals(0, ((List<Object>)result.get('errors')).size());
        System.assertEquals(title, resultWrapper.title);
        System.assertEquals(content, resultWrapper.content);
        System.assertEquals(1, resultWrapper.version);
    }

    @IsTest
    private static void shouldUpdateNote() {
        PS_Note__c note = NoteTestUtil.insertNote('Title note', 'Content note');

        String titleUpdate = 'Updated Note Title';
        String contentUpdate = 'Updated Note Content';
        NoteTestUtil.UpdateJSON reqBodyUpdate = new NoteTestUtil.UpdateJSON(note.Id, titleUpdate, contentUpdate);

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/';
        RestContext.request.httpMethod = 'PUT';
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(reqBodyUpdate));
        RestContext.response = new RestResponse();
        NoteService.updateNote();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        Object resultDataObject = (Object)result.get('data');
        NoteServiceWrappers.NoteWrapper resultWrapper = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(JSON.serialize(resultDataObject), NoteServiceWrappers.NoteWrapper.class);

        System.assert((Boolean)result.get('success'));
        System.assertEquals(0, ((List<Object>)result.get('errors')).size());
        System.assertEquals(titleUpdate, resultWrapper.title);
        System.assertEquals(contentUpdate, resultWrapper.content);
        System.assertEquals(2, resultWrapper.version);
    }

    @IsTest
    private static void shouldDeleteNote() {
        PS_Note__c note = NoteTestUtil.insertNote('Title note', 'Content note');
        NoteTestUtil.UpdateJSON reqBodyDelete = new NoteTestUtil.UpdateJSON(note.Id, '', '');


        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/';
        RestContext.request.httpMethod = 'DELETE';
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(reqBodyDelete));
        RestContext.response = new RestResponse();
        NoteService.deleteNote();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        Object resultDataObject = (Object)result.get('data');
        NoteServiceWrappers.NoteWrapper resultWrapper = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(JSON.serialize(resultDataObject), NoteServiceWrappers.NoteWrapper.class);

        System.assert((Boolean)result.get('success'));
        System.assertEquals(0, ((List<Object>)result.get('errors')).size());
        System.assert(resultWrapper.deleted);
    }

    @IsTest
    private static void shouldNotInsertBecauseRequiredFieldsAreMissing() {
        NoteServiceWrappers.NewNote reqBody = new NoteServiceWrappers.NewNote('', '');

        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/PSNotesService/';
        RestContext.request.httpMethod = 'POST';
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(reqBody));
        RestContext.response = new RestResponse();

        NoteService.createNote();

        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
        List<String> errorList = (List<String>)JSON.deserialize(JSON.serialize(result.get('errors')),List<String>.class);

        System.assertEquals(false, (Boolean)result.get('success'));
        System.assertEquals(2, errorList.size());
        System.assertEquals('Title required', errorList[0]);
        System.assertEquals('Content required', errorList[1]);
    }
}