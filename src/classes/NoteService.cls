/**
 * Created by ewasm on 15.09.2018.
 */
@RestResource(urlMapping='/PSNotesService/*')
global with sharing class NoteService {
    @HttpGet
    global static void getNotes() {
        NoteServiceWrappers.NotesServiceResult result = new NoteServiceWrappers.NotesServiceResult();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String noteId =  req.requestURI.substringAfterLast('PSNotesService').replace('/','');
        result.success = true;

        if (String.isNotEmpty(noteId)){
            try {
                result.data = new NoteServiceWrappers.NoteWrapper([SELECT Id, Title__c, Content__c, CreatedDate, Modified_Date__c, Version__c, Deleted__c
                                                                    FROM PS_Note__c
                                                                    WHERE id = :noteId AND Deleted__c = false]);
            }
            catch (Exception ex) {
                result.success = false;
                result.errors.add(ex.getMessage());
            }
        }
        else {
            result.data = new List<NoteServiceWrappers.NoteWrapper>();
            for (PS_Note__c note : [SELECT Id, Title__c, Content__c, CreatedDate, Modified_Date__c, Version__c, Deleted__c
                                    FROM PS_Note__c
                                    WHERE Deleted__c = false
                                    ORDER BY Modified_Date__c DESC]) {
                ((List<NoteServiceWrappers.NoteWrapper>)result.data).add(new NoteServiceWrappers.NoteWrapper(note));
            }
        }

        res.statusCode = result.success ? 200 : 500;
        res.responseBody = Blob.valueOf(JSON.serialize(result));
        res.addHeader('Content-Type', 'application/json');
    }

    @HttpPost
    global static void createNote() {
        NoteServiceWrappers.NotesServiceResult result = new NoteServiceWrappers.NotesServiceResult();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        result.success = true;
        res.statusCode = 201;
        try {
            NoteServiceWrappers.NewNote newNote = (NoteServiceWrappers.NewNote)JSON.deserializeStrict(req.requestBody.toString(), NoteServiceWrappers.NewNote.class);
            if (String.isBlank(newNote.title)) {
                result.success = false;
                result.errors.add('Title required');
                res.statusCode = 400;
            }
            if (String.isBlank(newNote.content)) {
                result.success = false;
                result.errors.add('Content required');
                res.statusCode = 400;
            }

            if (result.success) {
                PS_Note__c note = new PS_Note__c();
                note.Title__c = newNote.title;
                note.Content__c = newNote.content;
                note.Version__c = 1;
                note.Modified_Date__c = Datetime.now();
                insert note;
                result.data = new NoteServiceWrappers.NoteWrapper(note);
            }
        }
        catch (Exception ex) {
            result.success = false;
            result.errors.add(ex.getMessage());
            res.statusCode = 500;
        }

        res.responseBody = Blob.valueOf(JSON.serialize(result));
        res.addHeader('Content-Type', 'application/json');
    }

    @HttpPut
    global static void updateNote() {
        NoteServiceWrappers.NotesServiceResult result = new NoteServiceWrappers.NotesServiceResult();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        result.success = true;
        res.statusCode = 200;

        try {
            NoteServiceWrappers.NoteWrapper noteNewVersion = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(req.requestBody.toString(), NoteServiceWrappers.NoteWrapper.class);
            if (String.isBlank(noteNewVersion.id)) {
                result.success = false;
                result.errors.add('Id required');
                res.statusCode = 400;
            }
            else {
                Boolean shouldUpdate = false;
                PS_Note__c noteToUpdate = [SELECT Id, Title__c, Content__c, CreatedDate, Modified_Date__c, Version__c, Deleted__c
                                                FROM PS_Note__c
                                                WHERE id = :noteNewVersion.id AND Deleted__c = false];
                PS_Note__c oldVersion = noteToUpdate.clone(true);
                if (String.isNotBlank(noteNewVersion.title)) {
                    noteToUpdate.Title__c = noteNewVersion.title;
                    shouldUpdate = true;
                }
                if (String.isNotBlank(noteNewVersion.content)) {
                    noteToUpdate.Content__c = noteNewVersion.content;
                    shouldUpdate = true;
                }

                if (shouldUpdate) {
                    noteToUpdate.Version__c++;
                    noteToUpdate.Modified_Date__c = Datetime.now();
                    update noteToUpdate;

                    NoteVersioning noteVersioning = new NoteVersioning(oldVersion);
                    noteVersioning.archiveNoteVersion();
                    result.data = new NoteServiceWrappers.NoteWrapper(noteToUpdate);
                }
            }
        }
        catch (Exception ex) {
            result.success = false;
            result.errors.add(ex.getMessage());
            res.statusCode = 500;
        }

        res.responseBody = Blob.valueOf(JSON.serialize(result));
        res.addHeader('Content-Type', 'application/json');

    }

    @HttpDelete
    global static void deleteNote() {
        NoteServiceWrappers.NotesServiceResult result = new NoteServiceWrappers.NotesServiceResult();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        result.success = true;
        res.statusCode = 200;

        try {
            NoteServiceWrappers.NoteWrapper noteNewVersion = (NoteServiceWrappers.NoteWrapper)JSON.deserialize(req.requestBody.toString(), NoteServiceWrappers.NoteWrapper.class);
            if (String.isBlank(noteNewVersion.id)) {
                result.success = false;
                result.errors.add('Id required');
                res.statusCode = 400;
            }
            else {
                PS_Note__c noteToArchive = [SELECT Id, Title__c, Content__c, CreatedDate,
                                                    Modified_Date__c, Deleted__c, Version__c
                                            FROM PS_Note__c
                                            WHERE id = :noteNewVersion.id AND Deleted__c = false];

                noteToArchive.Deleted__c = true;
                update noteToArchive;
                result.data = new NoteServiceWrappers.NoteWrapper(noteToArchive);
            }
        }
        catch (Exception ex) {
            result.success = false;
            result.errors.add(ex.getMessage());
            res.statusCode = 500;
        }
        
        res.responseBody = Blob.valueOf(JSON.serialize(result));
        res.addHeader('Content-Type', 'application/json');
    }
}