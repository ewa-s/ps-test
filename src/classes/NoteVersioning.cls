/**
 * Created by ewasm on 15.09.2018.
 */

public with sharing class NoteVersioning {
    private PS_Note__c note;

    public NoteVersioning(PS_Note__c note) {
        this.note = note;
    }

    public void archiveNoteVersion() {
        PS_Note_History__c noteHistory = new PS_Note_History__c();
        noteHistory.Title__c = note.Title__c;
        noteHistory.Content__c = note.Content__c;
        noteHistory.Version__c = note.Version__c;
        noteHistory.Version_Created_Date__c = note.Modified_Date__c;
        noteHistory.Current_Note_Version__c = note.Id;

        insert noteHistory;
    }
}