/**
 * Created by ewasm on 16.09.2018.
 */

global with sharing class NoteTestUtil {
    global static PS_Note__c insertNote(String title, String content) {
        PS_Note__c note = new PS_Note__c();
        note.Title__c = title;
        note.Content__c = content;
        note.Version__c = 1;
        note.Modified_Date__c = Datetime.now();
        insert note;

        return note;
    }

    global static PS_Note_History__c insertNoteHistory(Id currentNoteId, String title, String content) {
        PS_Note_History__c noteHistory = new PS_Note_History__c();
        noteHistory.Current_Note_Version__c = currentNoteId;
        noteHistory.Title__c = title;
        noteHistory.Content__c = content;
        insert noteHistory;

        return noteHistory;
    }

    global class UpdateJSON {
        global Id id;
        global String title;
        global String content;

        global UpdateJSON(Id id, String title, String content) {
            this.id = id;
            this.title = title;
            this.content = content;
        }
    }
}