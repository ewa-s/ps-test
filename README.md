## Note Service
Implementation of RESTful API webservice responsible for managing and storing simple notes in Salesforce.

### Setup
1. Create new Salesforce developer org.
1. Set up Site domain
- go to `Setup->Sites`
- choose name and check availability
- register `My Salesforce Site Domain`
1. Create new Site. Fill in fields as following:
   `Name: Notes`
   `Label: Notes`
   `Active: checked`
   `Active Site Home Page : UnderConstruction`
1. In `src/sites/Notes.sites`
- change `<subdomain>ps-test-developer-edition</subdomain>` to `<subdomain>your-domain-name</subdomain>`
- set `<siteAdmin>ewa-pstest@smaga.biz.pl</siteAdmin>` to your user name
1. Zip src/ content
1. Log in to [https://workbench.developerforce.com](https://workbench.developerforce.com) with your developer org credentials and deploy zipped package

### How to use
- ##### Get all notes
|  | Value|
| --- | --- |
| Http method | `GET`|
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNotesService |
- ##### Get note by id
|  | Value|
| --- | --- |
| Http method | `GET` |
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNotesService/`note-id` |
- ##### Add note
|  | Value|
| --- | --- |
| Http method | `POST` |
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNotesService |
| Request body | `{` |
| |  `"title" : "Title"` |
| |  `"content" : "Content"` |
| | `}` |
- ##### Update note
|  | Value|
| --- | --- |
| Http method | `PUT` |
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNotesService |
| Request body | `{` |
| |  `"id" : "**note-id**"` |
| |  `"title" : "New title"` |
| |  `"content" : "New content"` |
| | `}` |
- ##### Delete note
|  | Value|
| --- | --- |
| Http method | `DELETE` |
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNotesService |
| Request body | `{` |
| |  `"id" : "**note-id**"` |
| | `}` |
- ##### Get note history
|  | Value|
| --- | --- |
| Http method | `GET` |
| Request URI | https://`your-domain`.eu16.force.com/services/apexrest/PSNoteHistoryService/`note-id` |

### Additional comments
Using in-memory database for unit tests assignment portion was not implemented because as far as I know Salesforce does not offer such functionality.

